# Drill Simulator

Provide a framework for running a disaster/communication drill to enhance
the real world aspects of the drill.

At the moment this project is for collecting ideas concerning building a
software product that can be used to automate and execute real world models
of an emergency or disaster scenario. While the idea is present, the
eventual product and technologies to be used are yet to be discovered.

The only way to position the idea is to think of the concept with respect
to game theory. In essence that is what the simulator would be is a game.
The rest of this description will be written from the point of view of
running a game as that should provide a more complete vocabulary for
describing the process the software product would take.

Everything would be initiated by creating a game by a game master (`GM`).
The `GM` would provide the initial parameters for the type of game. The
primary parameter to create the game would be which model to use for the
game. The following are initial thought on models to create:

- Snow / Wind storm
- Flooding
- Terrorist attack / Civil unrest
- Airliner crash
- Landslide
- High speed pursuit / Crash on I-90

Once the initial model is chosen there would undoubtly be parameters to be
specified to configure the model. Things like how much of the city is impacted,
how frequent events occur during game play, how responsive other
agencies/automous agents are, or enabling secondary events may be likely
parameters to be set in a model. Each model would supply its own parameters
that would need to be set.

The `GM` would also create a list of possible injects to be used during the
game play. These would likely take the form of communication messages from
virtual players in the form of other served agencies (city departments or
other local entities), parent agencies (county, state or federal agencies),
or even virtual teams working along side real world participants. There
also needs to be a mechanism for the inject to be modifiable by the game
during play to provide more realistic game play and unexpected outcomes.
This goes beyond a simple substituion of numeric data within an inject
(say requesting 40 beds instead of 15 in a shelter) and will likely be
model dependent.

More thought needs to go into what other game collateral may be needed.
Examples of other possible items include:

- Maps of areas being used in game play
- Annotations on maps to direct game play or scoring for likely events
  during game play (could there be multiple)
- ICS organizational charts
- Historical data from previous incidents

Once the game as been setup, the game is started by a game coordinator (`GC`).
It is the responsiblity of the `GC` to provide participant with generated
injects and update the game with the actions of the participants. There
could be multiple `GC`s during the game play. In some cases the `GC` may
play the role of an outside agency or other game element.

As the `GC` updates the game with participant actions, these actions would
be recorded in the incident log along with the generated actions/injects
during the game for analysis and after action reports.

Game play concludes when the alloted time expires or all objectives of
the game have been satisfied.

There are a number of open source simulation engines that could aid in the
development of the product. [List of engines](https://en.wikipedia.org/wiki/List_of_computer_simulation_software)
The following are a first pass of possible canidates:

- https://en.wikipedia.org/wiki/Mobility_Testbed
- https://en.wikipedia.org/wiki/NetLogo
- https://en.wikipedia.org/wiki/Repast_(modeling_toolkit)
- https://en.wikipedia.org/wiki/SimPy
- https://en.wikipedia.org/wiki/UrbanSim
- https://en.wikipedia.org/wiki/SIM.JS

