module.exports = mongoose => {
  const Drill = mongoose.model(
    "Drill",
    mongoose.Schema(
      {
        title: String,
        uuid: String,
        description: String,
        gamemaster: [
          {
            type: mongoose.Schema.Types.ObjectId,
            ref: "User"
          }
        ],
        start_time: Date,
        end_time: Date
      },
      { timestamps: true }
    )
  );
  return Drill;
};
