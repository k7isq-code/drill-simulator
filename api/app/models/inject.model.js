module.exports = mongoose => {
  const Inject = mongoose.model(
    "Inject",
    mongoose.Schema(
      {
        title: String,
        uuid: String,
        message: String,
        from: String,
        to: String
      },
      { timestamps: true }
    )
  );
  return Inject;
};
