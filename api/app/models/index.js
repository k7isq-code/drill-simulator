const dbConfig = require("../config/db.config.js");
const mongoose = require("mongoose");

mongoose.Promise = global.Promise;

// const db = {
//   mongoose: mongoose,
//   url: dbConfig.url,
//   role: require("./role.model.js")(mongoose),
//   ROLES: ['admin', 'gamemaster', 'player'],
//   user: require("./user.model.js")(mongoose),
//   inject: require("./inject.model.js")(mongoose),
// };

const db = { };
db.mongoose = mongoose;
db.url = dbConfig.url;
db.role = require("./role.model.js")(mongoose);
db.ROLES = ['admin', 'gamemaster', 'player'];
db.user = require("./user.model.js")(mongoose);
db.drill = require("./drill.model.js")(mongoose);
db.location = require("./location.model.js")(mongoose);
db.inject = require("./inject.model.js")(mongoose);

module.exports = db;
