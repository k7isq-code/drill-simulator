module.exports = mongoose => {
  const Location = mongoose.model(
    "Location",
    mongoose.Schema(
      {
        title: String,
        uuid: String,
        lat: Number,
        long: Number
      },
      { timestamps: true }
    )
  );
  return Location;
};
