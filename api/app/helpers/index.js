const authJwt = require('./authJwt');
const verifyRegister = require('./verify-register');
const logger = require('./logger');

module.exports = {
  authJwt,
  verifyRegister,
  logger,
};
