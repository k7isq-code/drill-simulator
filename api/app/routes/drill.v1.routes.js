const { logger } = require('../helpers');
const drills = require('../controllers/drill.controller');

module.exports = app => {

  logger.info("setting up drill routes");

  var router = require("express").Router();

  router.post("/", drills.create);
  router.get("/", drills.findAll);
  router.get("/:id", drills.findOne);
  router.put("/:id", drills.update);
  router.delete("/:id", drills.delete);
  router.delete("/", drills.deleteAll);

  app.use('/api/v1/drills', router);
};
