const { verifyRegister, logger } = require('../helpers');
const { body, validationResult } = require('express-validator');
const controller = require('../controllers/auth.controller');

module.exports = app => {

  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  logger.info("setting up auth routes");

  var router = require("express").Router();

  // router.post("/register", [
  //   verifyRegister.checkDuplicateUsernameOrEmail,
  //   verifyRegister.checkRolesExist], controller.register);
  router.post("/register", controller.register);
  router.post("/login", controller.login);

  app.use('/api/v1/auth', router);
};
