const { logger } = require('../helpers');

module.exports = app => {
  const injects = require("../controllers/inject.controller.js");

  logger.info("setting up inject routes");

  var router = require("express").Router();

  router.post("/", injects.create);
  router.get("/", injects.findAll);
  router.get("/:id", injects.findOne);
  router.put("/:id", injects.update);
  router.delete("/:id", injects.delete);
  router.delete("/", injects.deleteAll);

  app.use('/api/v1/injects', router);
};
