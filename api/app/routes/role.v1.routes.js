const { logger } = require('../helpers');
const controller = require('../controllers/role.controller');

module.exports = app => {

  // app.use(function(req, res, next) {
  //   res.header(
  //     "Access-Control-Allow-Headers",
  //     "x-access-token, Origin, Content-Type, Accept"
  //   );
  //   next();
  // });

  logger.info("setting up role routes");

  var router = require("express").Router();

  router.get("/", controller.getRoles);
  router.get("/:id", controller.getRole);
  router.put("/:id", controller.createRole);

  app.use('/api/v1/role', router);
};
