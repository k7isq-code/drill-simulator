const db = require('../models');
const Drill = db.drill;
const User = db.user;
const { logger, verifyToken,  } = require('../helpers');

// Create and Save a new Inject
exports.create = (req, res) => {
  logger.info("Calling drill.create");

  // Validate JWT
  verifyToken(req, res, () => {
    logger.info(req);
  });

  // Validate request
  if (!req.body.title) {
    res.status(400).send({ message: "Content can not be empty!" });
    return;
  }

  // Create a Drill
  const drill = new Drill({
    title: req.body.title,
    uuid: req.body.uuid ? req.body.uuid : '',
    description: req.body.description,
    gamemaster: [],
    start_time: Date(req.body.start_time),
    end_time: Date(req.body.end_time)
  });

  // Save Inject in the database
  drill
    .save(drill)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Drill."
      });
    });
};

// Retrieve all Injects from the database.
exports.findAll = (req, res) => {
  logger.info("called drill.findAll");

  const title = req.query.title;
  var condition = title ? { title: { $regex: new RegExp(title), $options: "i" } } : {};
  Drill.find(condition)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving Drills."
      });
    });
};

// Find a single Inject with an id
exports.findOne = (req, res) => {
  const id = req.params.id;
  Inject.findById(id)
    .then(data => {
      if (!data)
        res.status(404).send({ message: "Not found Inject with id " + id });
      else res.send(data);
    })
    .catch(err => {
      res
        .status(500)
        .send({ message: "Error retrieving Inject with id=" + id });
    });
};

// Update a Inject by the id in the request
exports.update = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!"
    });
  }
  const id = req.params.id;
  Inject.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot update Inject with id=${id}. Maybe Inject was not found!`
        });
      } else res.send({ message: "Inject was updated successfully." });
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Inject with id=" + id
      });
    });
};

// Delete a Inject with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;
  Inject.findByIdAndRemove(id)
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot delete Inject with id=${id}. Maybe Inject was not found!`
        });
      } else {
        res.send({
          message: "Inject was deleted successfully!"
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Inject with id=" + id
      });
    });
};

// Delete all Injects from the database.
exports.deleteAll = (req, res) => {
  Inject.deleteMany({})
    .then(data => {
      res.send({
        message: `${data.deletedCount} Injects were deleted successfully!`
      });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all Injects."
      });
    });
};

exports.findByUUID = (req, res) => {
  Inject.find({ uuid: req.body.uuid })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:`Some error occured while looking for ${req.body.uuid} UUID`
      });
    });
};

exports.findByFrom  = (req, res) => {
  Inject.find({ from: req.body.from })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:`Some error occured while looking for ${req.body.from} station`
      });
    });
};

