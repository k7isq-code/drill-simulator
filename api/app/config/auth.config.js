let jwtSecret = process.env['JWT_SECRET'];

module.exports = {
  secret: jwtSecret,
  session_timeout: 86400,   // 24 hours
  algorithm: 'RS256',
};
