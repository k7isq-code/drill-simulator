
let username   = process.env['MONGODB_USERNAME'];
let password   = process.env['MONGODB_PASSWORD'];
let hostname   = process.env['MONGODB_HOSTNAME'];
let port       = parseInt(process.env['MONGODB_PORT']) || 27017;
let dbname     = process.env['MONGODB_DBNAME'];

module.exports = {
  url: `mongodb+srv://${username}:${password}@${hostname}/${dbname}?retryWrites=true&w=majority`,
  dbName: dbname,
};

