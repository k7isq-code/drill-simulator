const models = require('../app/models');
const bcrypt = require('bcryptjs');

module.exports = {
  async up(db, client) {
    admin_role = await db.collection('roles').findOne({ name: 'admin' });
    admin_user = new models.user({
      username: 'admin',
      email: 'wt0f@arrl.net',
      password: bcrypt.hashSync('foobar', 8),
      roles: [ admin_role.id ]
    })
    db.collection('users').insertOne(admin_user);
  },

  async down(db, client) {
    db.collection('users').deleteOne({ username: 'admin' });
  }
};
