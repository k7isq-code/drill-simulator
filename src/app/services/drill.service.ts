import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Drill } from '../models/drill.model';
import {  throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})

export class DrillService {
  baseUrl = 'http://localhost:8080/api/v1/drills';

  constructor(private http: HttpClient) { }

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  getAll(): Observable<Drill[]> {
    return this.http.get<Drill[]>(this.baseUrl).pipe(retry(3), catchError(this.handleError));
  }

  get(id: any): Observable<Drill> {
    return this.http.get<Drill>(`${this.baseUrl}/${id}`).pipe(retry(3), catchError(this.handleError));
  }

  create(data: any): Observable<any> {
    console.log(this.baseUrl);
    console.log(data);
    return this.http.post(this.baseUrl, data).pipe(retry(3), catchError(this.handleError));
  }

  update(id: any, data: any): Observable<any> {
    return this.http.put(`${this.baseUrl}/${id}`, data).pipe(retry(3), catchError(this.handleError));
  }

  delete(id: any): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`).pipe(retry(3), catchError(this.handleError));
  }

  deleteAll(): Observable<any> {
    return this.http.delete(this.baseUrl).pipe(retry(3), catchError(this.handleError));
  }

  findByTitle(title: any): Observable<Drill[]> {
    return this.http.get<Drill[]>(`${this.baseUrl}?title=${title}`).pipe(retry(3), catchError(this.handleError));
  }
}
