import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  AUTH_API = 'http://localhost:8080/api/v1/auth/';
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient) { }

  login(username: string, password: string): Observable<any> {
    return this.http.post(this.AUTH_API + 'login', {
      username,
      password
    }, this.httpOptions);
  }

  register(username: string, email: string, password: string, roles: string[]): Observable<any> {
    return this.http.post(this.AUTH_API + 'register', {
      username,
      email,
      password,
      roles
    }, this.httpOptions);
  }
}
