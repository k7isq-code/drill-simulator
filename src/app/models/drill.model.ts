import { User } from "./user.model";

export class Drill {
  id?: any;
  title?: string;
  uuid?: string;
  description?: string;
  gamemaster?: User[];
  start_time?: Date;
  end_time?: Date;
}
