export class Location {
  id?: any;
  name?: string;
  uuid?: string;
  lat?: number;
  long?: number;
}
