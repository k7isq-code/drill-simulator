export class Inject {
  id?: any;
  title?: string;
  uuid?: string;
  message?: string;
  from?: string;
  to?: string;
}
