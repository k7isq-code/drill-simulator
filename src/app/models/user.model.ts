import { Role } from "./role.model";

export class User {
  id?: any;
  username?: string;
  email?: string;
  password?: string;
  roles?: Role[];
}
