import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';


import { AboutComponent } from './components/about/about.component';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { ProfileComponent } from './components/profile/profile.component';

import { AddDrillComponent } from './components/drill/add-drill.component';
import { DrillsListComponent } from './components/drill/drills-list.component';
import { DrillDetailsComponent } from './components/drill/drill-details.component';

import { AddLocationComponent } from './components/location/add-location.component';
import { LocationsListComponent } from './components/location/locations-list.component';
import { LocationDetailsComponent } from './components/location/location-details.component';

import { AddInjectComponent } from './components/inject/add-inject.component';
import { InjectsListComponent } from './components/inject/injects-list.component';
import { InjectDetailsComponent } from './components/inject/inject-details.component';
import { InjectViewComponent } from './components/inject/inject-view.component';

import { DrillService } from './services/drill.service';
import { InjectService } from './services/inject.service';
import { LocationService } from './services/location.service';

@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    RegisterComponent,
    LoginComponent,
    ProfileComponent,
    AddDrillComponent,
    DrillsListComponent,
    DrillDetailsComponent,
    AddLocationComponent,
    LocationsListComponent,
    LocationDetailsComponent,
    AddInjectComponent,
    InjectsListComponent,
    InjectDetailsComponent,
    InjectViewComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
  ],
  providers: [
    DrillService,
    InjectService,
    LocationService,
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
