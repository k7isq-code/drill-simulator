import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AboutComponent } from './components/about/about.component';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { ProfileComponent } from './components/profile/profile.component';
import { AddDrillComponent } from './components/drill/add-drill.component';
import { DrillsListComponent } from './components/drill/drills-list.component';
import { DrillDetailsComponent } from './components/drill/drill-details.component';
import { AddLocationComponent } from './components/location/add-location.component';
import { LocationsListComponent } from './components/location/locations-list.component';
import { LocationDetailsComponent } from './components/location/location-details.component';
import { AddInjectComponent } from './components/inject/add-inject.component';
import { InjectsListComponent } from './components/inject/injects-list.component';
import { InjectDetailsComponent } from './components/inject/inject-details.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', component: AboutComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'profile', component: ProfileComponent },

  { path: 'drills', component: DrillsListComponent },
  { path: 'drills/add', component: AddDrillComponent },
  { path: 'drills/:id', component: DrillDetailsComponent },

  { path: 'locations', component: LocationsListComponent },
  { path: 'locations/add', component: AddLocationComponent },
  { path: 'locations/:id', component: LocationDetailsComponent },

  { path: 'injects', component: InjectsListComponent },
  { path: 'injects/add', component: AddInjectComponent },
  { path: 'injects/:id', component: InjectDetailsComponent },

];


// const routes: Routes = [
//   { path: 'home', component: HomeComponent },
//   { path: 'user', component: BoardUserComponent },
//   { path: 'mod', component: BoardModeratorComponent },
//   { path: 'admin', component: BoardAdminComponent },
//   { path: '', redirectTo: 'home', pathMatch: 'full' }
// ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
