import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InjectViewComponent } from './inject-view.component';

describe('InjectViewComponent', () => {
  let component: InjectViewComponent;
  let fixture: ComponentFixture<InjectViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InjectViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InjectViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
