import { Component, OnInit } from '@angular/core';
import { Inject } from '../../models/inject.model';
import { InjectService } from '../../services/inject.service';

@Component({
  selector: 'app-add-inject',
  templateUrl: './add-inject.component.html',
  styleUrls: ['./add-inject.component.sass']
})

export class AddInjectComponent implements OnInit {
  inject: Inject = {
    title: '',
    uuid: '',
    message: '',
    from: '',
    to: ''
  };

  submitted = false;

  constructor(private injectService: InjectService) { }

  ngOnInit(): void {
  }

  saveInject(): void {
    const data = {
      title: this.inject.title,
      uuid: this.inject.uuid,
      message: this.inject.message,
      from: this.inject.from,
      to: this.inject.to
    };

    this.injectService.create(data)
      .subscribe({
        next: (res) => {
          console.log(res);
          this.submitted = true;
        },
        error: (e) => console.error(e)
      });
  }

  newInject(): void {
    this.submitted = false;
    this.inject = {
      title: '',
      uuid: '',
      message: '',
      from: '',
      to: ''
    };
  }
}
