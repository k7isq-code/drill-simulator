import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InjectDetailsComponent } from './inject-details.component';

describe('InjectDetailsComponent', () => {
  let component: InjectDetailsComponent;
  let fixture: ComponentFixture<InjectDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InjectDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InjectDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
