import { Component, OnInit } from '@angular/core';
import { Inject } from 'src/app/models/inject.model';
import { InjectService } from 'src/app/services/inject.service';

@Component({
  selector: 'app-injects-list',
  templateUrl: './injects-list.component.html',
  styleUrls: ['./injects-list.component.sass']
})

export class InjectsListComponent implements OnInit {
  injects?: Inject[];
  currentInject: Inject = {};
  currentIndex = -1;
  title = '';

  constructor(private injectService: InjectService) { }

  ngOnInit(): void {
    this.retrieveInjects();
  }

  retrieveInjects(): void {
    this.injectService.getAll()
      .subscribe({
        next: (data) => {
          this.injects = data;
          console.log(data);
        },
        error: (e) => console.error(e)
      });
  }

  refreshList(): void {
    this.retrieveInjects();
    this.currentInject = {};
    this.currentIndex = -1;
  }

  setActiveInject(inject: Inject, index: number): void {
    this.currentInject = inject;
    this.currentIndex = index;
  }

  addNewInject(): void {
    this.injectService.deleteAll()
      .subscribe({
        next: (res) => {
          console.log(res);
          this.refreshList();
        },
        error: (e) => console.error(e)
      });
  }

  searchTitle(): void {
    this.currentInject = {};
    this.currentIndex = -1;
    this.injectService.findByTitle(this.title)
      .subscribe({
        next: (data) => {
          this.injects = data;
          console.log(data);
        },
        error: (e) => console.error(e)
      });
  }
}
