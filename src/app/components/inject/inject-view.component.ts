import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inject-view',
  templateUrl: './inject-view.component.html',
  styleUrls: ['./inject-view.component.sass']
})
export class InjectViewComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
