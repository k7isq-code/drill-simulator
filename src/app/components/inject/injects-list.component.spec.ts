import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InjectsListComponent } from './injects-list.component';

describe('InjectsListComponent', () => {
  let component: InjectsListComponent;
  let fixture: ComponentFixture<InjectsListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InjectsListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InjectsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
