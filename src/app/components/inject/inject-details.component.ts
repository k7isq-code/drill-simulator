import { Component, Input, OnInit } from '@angular/core';
import { Inject } from 'src/app/models/inject.model';
import { InjectService } from 'src/app/services/inject.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-inject-details',
  templateUrl: './inject-details.component.html',
  styleUrls: ['./inject-details.component.sass']
})

export class InjectDetailsComponent implements OnInit {
  @Input() viewMode = false;
  @Input() currentInject: Inject = {
    title: '',
    message: ''
  };

  message = '';
  constructor(
    private injectService: InjectService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    if (!this.viewMode) {
      this.message = '';
      this.getInject(this.route.snapshot.params["id"]);
    }
  }
  getInject(id: string): void {
    this.injectService.get(id)
      .subscribe({
        next: (data) => {
          this.currentInject = data;
          console.log(data);
        },
        error: (e) => console.error(e)
      });
  }
  updatePublished(status: boolean): void {
    const data = {
      title: this.currentInject.title,
      description: this.currentInject.message
    };

    this.message = '';
    this.injectService.update(this.currentInject.id, data)
      .subscribe({
        next: (res) => {
          console.log(res);
          this.message = res.message ? res.message : 'The status was updated successfully!';
        },
        error: (e) => console.error(e)
      });
  }

  updateInject(): void {
    this.message = '';
    this.injectService.update(this.currentInject.id, this.currentInject)
      .subscribe({
        next: (res) => {
          console.log(res);
          this.message = res.message ? res.message : 'This inject was updated successfully!';
        },
        error: (e) => console.error(e)
      });
  }

  deleteInject(): void {
    this.injectService.delete(this.currentInject.id)
      .subscribe({
        next: (res) => {
          console.log(res);
          this.router.navigate(['/injects']);
        },
        error: (e) => console.error(e)
      });
  }
}
