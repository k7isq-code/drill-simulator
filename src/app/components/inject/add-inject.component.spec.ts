import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddInjectComponent } from './add-inject.component';

describe('AddInjectComponent', () => {
  let component: AddInjectComponent;
  let fixture: ComponentFixture<AddInjectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddInjectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddInjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
