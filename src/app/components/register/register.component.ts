import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.sass']
})
export class RegisterComponent implements OnInit {
  form: any = {
    username: null,
    email: null,
    password: null,
    roles: ['player'],
  };

  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
  }

  onSubmit(): void {
    const { username, email, password, roles } = this.form;
    console.log(JSON.stringify(this.form));

    this.authService.register(username, email, password, roles).subscribe({
      next: data => {
        console.log(JSON.stringify(data));
        this.isSuccessful = true;
        this.isSignUpFailed = false;
      },
      error: err => {
        console.log(err.error.message);
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
      }
    });
  }
}
