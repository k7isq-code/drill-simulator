import { Component, OnInit } from '@angular/core';
import { Drill } from '../../models/drill.model';
import { DrillService } from '../../services/drill.service';

@Component({
  selector: 'app-add-drill',
  templateUrl: './add-drill.component.html',
  styleUrls: ['./add-drill.component.sass']
})

export class AddDrillComponent implements OnInit {
  drill: Drill = {
    title: '',
    uuid: '',
    description: '',
    start_time: undefined,
    end_time: undefined
  };

  submitted = false;

  constructor(private drillService: DrillService) { }

  ngOnInit(): void {
  }

  saveDrill(): void {
    const data = {
      title: this.drill.title,
      uuid: this.drill.uuid,
      description: this.drill.description,
      start_time: this.drill.start_time,
      end_time: this.drill.end_time
    };

    this.drillService.create(data)
      .subscribe({
        next: (res) => {
          console.log(res);
          this.submitted = true;
        },
        error: (e) => console.error(e)
      });
  }

  newDrill(): void {
    this.submitted = false;
    this.drill = {
      title: '',
      uuid: '',
      description: '',
      start_time: undefined,
      end_time: undefined
    };
  }
}
