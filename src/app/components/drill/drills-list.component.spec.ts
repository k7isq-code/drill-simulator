import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DrillsListComponent } from './drills-list.component';

describe('DrillsListComponent', () => {
  let component: DrillsListComponent;
  let fixture: ComponentFixture<DrillsListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DrillsListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DrillsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
