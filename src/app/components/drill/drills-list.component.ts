import { Component, OnInit } from '@angular/core';
import { Drill } from 'src/app/models/drill.model';
import { DrillService } from 'src/app/services/drill.service';

@Component({
  selector: 'app-drils-list',
  templateUrl: './drills-list.component.html',
  styleUrls: ['./drills-list.component.sass']
})

export class DrillsListComponent implements OnInit {
  drills?: Drill[];
  currentDrill: Drill = {};
  currentIndex = -1;
  title = '';

  constructor(private drillService: DrillService) { }

  ngOnInit(): void {
    this.retrieveDrills();
  }

  retrieveDrills(): void {
    this.drillService.getAll()
      .subscribe({
        next: (data) => {
          this.drills = data;
          console.log(data);
        },
        error: (e) => console.error(e)
      });
  }

  refreshList(): void {
    this.retrieveDrills();
    this.currentDrill = {};
    this.currentIndex = -1;
  }

  setActiveDrill(drill: Drill, index: number): void {
    this.currentDrill = drill;
    this.currentIndex = index;
  }

  addNewDrill(): void {
    this.drillService.deleteAll()
      .subscribe({
        next: (res) => {
          console.log(res);
          this.refreshList();
        },
        error: (e) => console.error(e)
      });
  }

  searchTitle(): void {
    this.currentDrill = {};
    this.currentIndex = -1;
    this.drillService.findByTitle(this.title)
      .subscribe({
        next: (data) => {
          this.drills = data;
          console.log(data);
        },
        error: (e) => console.error(e)
      });
  }
}
