import { Component, Input, OnInit } from '@angular/core';
import { Drill } from 'src/app/models/drill.model';
import { DrillService } from 'src/app/services/drill.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-drill-details',
  templateUrl: './drill-details.component.html',
  styleUrls: ['./drill-details.component.sass']
})

export class DrillDetailsComponent implements OnInit {
  @Input() viewMode = false;
  @Input() currentDrill: Drill = {
    title: '',
    description: ''
  };

  description = '';
  constructor(
    private drillService: DrillService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    if (!this.viewMode) {
      this.description = '';
      this.getDrill(this.route.snapshot.params["id"]);
    }
  }
  getDrill(id: string): void {
    this.drillService.get(id)
      .subscribe({
        next: (data) => {
          this.currentDrill = data;
          console.log(data);
        },
        error: (e) => console.error(e)
      });
  }
  updatePublished(status: boolean): void {
    const data = {
      title: this.currentDrill.title,
      description: this.currentDrill.description
    };

    this.description = '';
    this.drillService.update(this.currentDrill.id, data)
      .subscribe({
        next: (res) => {
          console.log(res);
          this.description = res.description ? res.description : 'The status was updated successfully!';
        },
        error: (e) => console.error(e)
      });
  }

  updateDrill(): void {
    this.description = '';
    this.drillService.update(this.currentDrill.id, this.currentDrill)
      .subscribe({
        next: (res) => {
          console.log(res);
          this.description = res.description ? res.description : 'This drill was updated successfully!';
        },
        error: (e) => console.error(e)
      });
  }

  deleteDrill(): void {
    this.drillService.delete(this.currentDrill.id)
      .subscribe({
        next: (res) => {
          console.log(res);
          this.router.navigate(['/drills']);
        },
        error: (e) => console.error(e)
      });
  }
}
